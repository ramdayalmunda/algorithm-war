import HomeComponent from "./components/home/home.js" 
const routes = [
    { path: "", name: "Home", component: HomeComponent },
    { path: "/war", name: "War", component: ()=> import("./components/war/war.js") },
    { path: "/admin", name: "Admin", component: ()=> import("./components/admin/admin.js") },
]

const router = VueRouter.createRouter({
    routes,
    history: VueRouter.createWebHistory()
})


export default router