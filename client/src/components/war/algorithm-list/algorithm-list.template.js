export default `


<table>
    <thead>
        <tr class="fw-bold">
            <td>#</td>
            <td>Name</td>
            <td>Score</td>
            <td v-if="columnList.includes('load')">Action</td>
        </tr>
    </thead>
    <tbody>
        <template v-for=" (alg, a) in algorithmList" :key="alg._id">
            <tr :title="(!alg.validated?'**not approved by admins yet**\\r\\n':'')+alg.algStr">
                <td>{{a+1}}</td>
                <td>{{alg.name}}</td>
                <td>{{alg.score}}</td>
                <td v-if="columnList.includes('load')">
                    <button v-if="columnList.includes('load')" @click="sendAlgorithmData(alg)">Load</button>
                </td>
            </tr>
        </template>
    </tbody>
</table>

`