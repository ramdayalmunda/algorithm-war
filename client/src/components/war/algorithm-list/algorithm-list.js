import api from "../../../common/api.js"
import template from "./algorithm-list.template.js"

const { ref, onMounted } = Vue;

export default {
    template,
    props:{
        columnList: Array,
    },
    setup(props, { emit }){
        let defaultCoumnList = [ "#", "Name", "Score" ]
        if (!props?.columnList?.length) props.columnList =  [ ...defaultCoumnList ]

        let algorithmList = ref([])
        async function getAlgorithmList(){
            try{
                let response = await api('GET', '/algorithm/all')
                algorithmList.value = response.list
            }catch(err){
                console.log(err)
            }
        }

        function sendAlgorithmData(data){
            emit('send-algorithm-data', data)
        }

        onMounted( async()=>{
            getAlgorithmList()
        } )

        return {
            algorithmList,
            getAlgorithmList,
            sendAlgorithmData,
        }
    }
}