export default `



<div class="button-bar">
    <button @click="startTournament()">Start Tournament</button>
</div>
<template v-for="(match, m) in matches" :key="match._id">
    <table>
        <thead>
            <tr>
                <td class="justify-content-between cursor-pointer" @click="openedMatch=openedMatch==match._id?'':match._id">
                    <span>{{match.players[0].name}}:{{match.players[0].score}}</span>
                    <span>vs</span>
                    <span>{{match.players[1].score}}:{{match.players[1].name}}</span>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="collapsible" :class="openedMatch==match._id?'':'closed'">
                    <table>
                        <thead>
                            <tr class="fw-bold">
                                <td>Round</td>
                                <td class="t-e">{{match.players[0].name}}</td>
                                <td class="t-s">{{match.players[1].name}}</td>
                            </tr>
                        </thead>
                        <tbody>
                            <template v-for="( round, r ) in match.rounds">
                                <tr>
                                    <td>{{r+1}}</td>
                                    <td class="t-e">
                                       <span>{{round[0]==round[1]?( round[0]?'+3':'--' ):(round[0]?'--':'+5')}}</span> <span> {{round[0]?'help':'fight'}} </span>
                                    </td>
                                    <td class="t-s">
                                        <span> {{round[1]?'help':'fight'}} </span> <span>{{round[0]==round[1]?( round[1]?'+3':'--' ):(round[1]?'--':'+5')}}</span> 
                                    </td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</template>



`