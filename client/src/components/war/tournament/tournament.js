import api from "../../../common/api.js"
import template from "./tournament.template.js"

const { ref, onMounted } = Vue;

export default {
    template,
    setup(){

        let inProgress =ref(false)
        let matches = ref([])
        let openedMatch = ref("")

        async function startTournament(){
            try{
                inProgress.value = true
                let response = await api( 'POST', '/tournament/start' )
                await getScores()
                
            }catch(err){
                console.log(err)
            }
            inProgress.value = false
        }

        async function getScores(){
            try{
                let response = await api('GET', '/tournament/scores')
                matches.value = response.matches
                if (matches.value.length) openedMatch.value = matches.value[0]._id
            }catch(err){
                console.log(err)
            }
        }

        onMounted( async()=>{
            console.log('mounting Tournamets')
            getScores()
        } )

        return {
            inProgress,
            matches,
            openedMatch,
            startTournament,
        }
    }
}