import api from "../../../common/api.js";
import template from "./test.template.js"

const { ref, onBeforeMount } = Vue;
export default {
    template,
    setup() {
        let matchData = ref({
            players: [
                { id: "", name: "", score: 0 },
                { id: "", name: "", score: 0 },
            ],
            rounds: [],
            scores: [],
        })
        let roundIndex = 0;
        let roundTimeout = null
        let matchInProgress = ref(false)
        let algorithmList = ref([])
        let totalRounds = ref(100)

        var playerIndex = 0
        let gameObj = {
            getRoundIndex() { return roundIndex },
            getMyResponse(n) {
                if (typeof n == 'undefined') n = roundIndex - 1
                return matchData.value.rounds[n][playerIndex]
            },
            getOpponentResponse(n) {
                if (typeof n == 'undefined') n = roundIndex - 1
                return matchData.value.rounds?.[n]?.[playerIndex ? 0 : 1]
            },
        }

        function resetMatchData() {
            matchData.value.players[0] = { id: "", name: "", score: 0 }
            matchData.value.players[1] = { id: "", name: "", score: 0 }
            matchData.value.rounds = []
        }

        async function getAlgorithmList() {
            try {
                let response = await api('GET', '/algorithm/all')
                algorithmList.value = response.list
            } catch (err) {
                console.log(err)
            }
        }

        function changePlayer(playerData){
            let algorithmData = algorithmList.value.find( item => item._id==playerData.id )
            playerData.name = algorithmData.name
        }

        function startMatch() {
            if (!matchData.value.players[0].id || !matchData.value.players[1].id) {
                alert('Please select 2 algorithms to start the match')
                return
            }
            matchInProgress.value = true
            roundIndex = 0
            playerIndex = 0
            matchData.value.rounds = []
            matchData.value.scores = []
            matchData.value.players[0].score = 0
            matchData.value.players[1].score = 0
            matchData.value.players[0].alg = (algorithmList.value.find(item => item._id == matchData.value.players[0].id)).algStr
            matchData.value.players[1].alg = (algorithmList.value.find(item => item._id == matchData.value.players[1].id)).algStr
            recurvieRounds()
        }

        function recurvieRounds() {

            playerIndex = 0;
            let p0Decision = getResultOfAlgorithm(matchData.value.players[0].alg)
            playerIndex = 1
            let p1Decision = getResultOfAlgorithm(matchData.value.players[1].alg)

            let score = [
                (p0Decision==p1Decision)?(p0Decision?3:0):(p0Decision?0:5), 
                (p0Decision==p1Decision)?(p1Decision?3:0):(p1Decision?0:5)
            ]
            matchData.value.scores.push(score)
            matchData.value.players[0].score += score[0]
            matchData.value.players[1].score += score[1]
            ++roundIndex
            if (roundIndex < (totalRounds.value)) { roundTimeout = setTimeout(recurvieRounds, 500) }
            else {
                matchInProgress.value = false
            }
            matchData.value.rounds.push([ p0Decision, p1Decision ])
        }

        function getResultOfAlgorithm(str) {
            try {
                window.algorithm = null
                let newStr = `${str}\r\n\r\n algorithm( gameObj )`
                let res = eval( newStr );
                return res
            } catch (error) {
                console.error('Error occurred:', error);
                return undefined;
            }
        }

        function validateNumber(e){
            let str = e.target.value
            let num = str.replace(/\D/g, '');
            if (!num) num = 0
            num = parseInt(num)
            totalRounds.value = num
        }

        onBeforeMount(() => {
            getAlgorithmList()
        })

        return {
            totalRounds,
            matchData,
            matchInProgress,
            algorithmList,
            resetMatchData,
            startMatch,
            changePlayer,
            validateNumber,
        }
    }
}