export default `
<fieldset :disabled="matchInProgress">
    <div class="container-item">
        <div class="justify-content-between">
            <select v-model="matchData.players[0].id" @change="changePlayer(matchData.players[0])">
                <option value="" disabled>Choose player 1</option>
                <template v-for="(alg, a) in algorithmList" :key="'0-'+alg._id">
                    <option :value="alg._id">{{alg.name}}</option>
                </template>
            </select>
            <div class="container-row">
                <span class="t-c">Total Rounds</span>
                <input class="t-c" v-model="totalRounds" @input="validateNumber"/>
            </div>
            <select v-model="matchData.players[1].id" @change="changePlayer(matchData.players[1])">
                <option value="" disabled>Choose player 1</option>
                <template v-for="(alg, a) in algorithmList" :key="'1-'+alg._id">
                    <option :value="alg._id">{{alg.name}}</option>
                </template>
            </select>
        </div>
    </div>
    <div class="container-item">
        <button @click="startMatch">Start Match</button>
    </div>
    <table v-if="matchData?.players[0].name && matchData?.players[1]?.name">

        <thead>
            <tr class="fw-bold">
                <td style="width: 20%;">#</td>
                <td style="width: 40%;">
                    <div class="justify-content-between">
                        <span>{{matchData?.players[0]?.name}}</span>
                        <span>{{matchData?.players[0]?.score}}</span>
                    </div>
                </td>
                <td style="width: 40%;">
                    <div class="justify-content-between">
                        <span>{{matchData?.players[1]?.score}}</span>
                        <span>{{matchData?.players[1]?.name}}</span>
                    </div>
                </td>
            </tr>
        </thead>
        <tbody>
            <template v-if="!matchData?.rounds?.length">
                <tr>
                    <td colspan="3" class="t-c">No rounds played</td>
                </tr>    
            </template>
            <template v-else>
                <tr v-for="(round, r) in matchData?.rounds">
                    <td>{{r+1}}</td>
                    <td>
                        <div class="justify-content-between">
                            <span>{{round[0]?'help':'fight'}} </span>
                            <span> {{matchData?.scores?.[r][0]?('+'+matchData?.scores?.[r][0]):'--'}}</span>
                        </div>
                    </td>
                    <td>
                        <div class="justify-content-between">
                            <span>{{matchData?.scores?.[r][1]?('+'+matchData?.scores?.[r][1]):'--'}} </span>
                            <span> {{round[1]?'help':'fight'}}</span>
                        </div>
                    </td>
                </tr>
            </template>
        </tbody>
    </table>
</fieldset>

`