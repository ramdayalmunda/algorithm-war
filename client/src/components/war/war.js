import template from "./war.template.js"

import AlgorithmList from "./algorithm-list/algorithm-list.js";
import Tournament from "./tournament/tournament.js";
import Test from "./test/test.js"
import api from "../../common/api.js";
import { debounce } from "../../common/helper.js";

const { ref, onBeforeMount, onBeforeUnmount } = Vue;
const { useRouter, useRoute } = VueRouter
export default {
    template,
    components: {
        AlgorithmList,
        Tournament,
        "Custom Match":Test,
    },
    setup() {
        let router = useRouter();
        let route = useRoute()
        let inProgress = ref(false);
        let algorithmData = ref({
            name: '',
            password: "",
            algStr: "\r\nfunction algorithm (gameObj){\r\n\r\n  return true;\r\n\r\n};"
        })
        let localStoreDone = ref(false);
        let characterLimit = 2000
        let editorRef = ref(null);

        let helpText = `
Create a function named "algorithm" that will return either true or false

Your algorithm will be matched with other algorithms for 200 rounds.
Each round both the functions would be called, and a Boolean value is expected.
Your function must
    return true if you want to help
    return false if you want to fight

If both the function decide to help. Both will earn 2 points
If one function fights and other function helps
    -The function who fights would gain 5 points
    -The function who helps will not gain anything
If both the functions decide to fight.
    -No one will get any points

The function will have only 1 paramter of type object.

This Object will have the following methods available for you
1. getMyResponse()-> Boolean; this returns your response in the last round
2. getMyResponse(n)-> Boolean; this returns your response in the nth round
3. getOpponentResponse() -> Boolean; this returns your opponent's response in the last round
4. getOpponentResponse(n) -> Boolean; this returns your opponent's response in the nth round
5. getRoundIndex() -> Number; This will return the number of round you are playing in this match

        `;

        let helpVisible = ref(false)

        let viewList = ref([
            "AlgorithmList",
            "Tournament",
            "Custom Match",
        ])
        let viewName = ref(route.query.tab?route.query.tab:"AlgorithmList");
        let viewRef = ref(null);

        const storeCodeLocally = function(){
            algorithmData.value.name = algorithmData.value.name.trim()
            localStorage.setItem( 'algorithmData', JSON.stringify(algorithmData.value) )
            localStoreDone.value = true;
            setTimeout( ()=>{ localStoreDone.value = false }, 1000 )
        }
        const storeCodeLocallyDebounce = debounce( storeCodeLocally, 1000)


        const loadLocalCode = function (){
            let localData = localStorage.getItem('algorithmData' )
            if (localData){
                localData = JSON.parse( localData )
                if (localData){
                    algorithmData.value.name = localData.name
                    algorithmData.value.password = localData.password
                    algorithmData.value.algStr = localData.algStr
                    if (localData._id) algorithmData.value._id = localData._id
                }
            }
        }


        async function submitAlgorithm() {
            console.log('submitAlgorithm', algorithmData.value)
            inProgress.value = true
            try {
                algorithmData.value.name = algorithmData.value.name.trim()
                if (!algorithmData.value.name) {
                    alert("Enter a name for your algorithm");
                    return
                }
                if (!algorithmData.value.algStr) {
                    alert("Cannot submit an empty algorithm");
                    return
                }

                if (algorithmData.value.algStr.length > characterLimit){
                    alert('The code has exceeded the length of 1000')
                    return
                }
                let response = await api("POST", "/algorithm", { data: algorithmData.value })
                if (response.success){
                    console.log('success')
                    algorithmData.value._id = response.algorithmData._id
                    storeCodeLocally()
                }
                console.log(response)
            } catch (err) {
                console.log(err)
            }
            inProgress.value = false
        }

        function changeTab(newTabName){
            viewName.value = newTabName
            router.replace({ query: { tab: newTabName } })
        }

        function hideHelp(e){
            if (e.key === 'Escape') {
                helpVisible.value = false
            }
        }

        onBeforeMount( async function(){
            loadLocalCode()
            window.addEventListener( 'keydown', hideHelp )
        } )
        onBeforeUnmount( async function(){
            window.removeEventListener('keydown', hideHelp)
        } )

        return {
            helpVisible,
            editorRef,
            characterLimit,
            helpText,
            inProgress,
            algorithmData,
            viewList,
            viewName,
            viewRef,
            localStoreDone,
            submitAlgorithm,
            storeCodeLocallyDebounce,
            changeTab,
        }

    }
}