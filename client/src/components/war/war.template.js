export default `

<div class="container">

    <div class="left-div">
        <div class="container-row" v-if="helpVisible">
            <div class="container-item">
                <div class="header">Help is here. <span class="icon" @click="helpVisible=!helpVisible">X</span></div>
            </div>
            <div class="container-item">
                <p>Create a function named "algorithm" that will return either true or false<br/>
                <br/>Your algorithm will be matched with other algorithms for 200 rounds.<br/>
                <br/>Each round both the functions would be called, and a Boolean value is expected.</p>
                <p>Your function must<br/>
                    <ul>
                        <li>return true if you want to help</li>
                        <li>return false if you want to fight</li>
                    </ul>
                </p>
                <p>If both the function decide to help. Both will earn 2 points<br/>
                <br/>If one function fights and other function helps<br/>
                    <ul>
                        <li>The function who fights would gain 5 points</li>
                        <li>The function who helps will not gain anything</li>
                    </ul>
                </p>
                
                <p>If both the functions decide to fight.<br/>
                    <ul>
                        <li>No one will get any points</li>
                    </ul>
                </p>
                <p>The function will have only 1 paramter of type object.<br/>
                <br/>This Object will have the following methods available for you
                    <ul>
                        <li>getMyResponse()  -> Boolean; this returns your response in the last round</li>
                        <li>getMyResponse(n)  -> Boolean; this returns your response in the nth round</li>
                        <li>getOpponentResponse()  -> Boolean; this returns your opponent's response in the last round</li>
                        <li>getOpponentResponse(n)  -> Boolean; this returns your opponent's response in the nth round</li>
                        <li>getRoundIndex()  -> Number; This will return the number of round you are playing in this match</li>
                    </ul>
                </p>


            </div>
        </div>
        <div class="container-row" v-else>
            <div class="container-item">
                <div class="header">Write your Algorithm <span class="fs-small animate-fade"
                        :class="localStoreDone?'show':''"> Saving...</span></div>
            </div>
            <div class="container-item">
                <input type="text" name="name-of-algorithm" id="name-of-algorithm" @input="storeCodeLocallyDebounce"
                    placeholder="name for your algorithm" v-model="algorithmData.name">
                <input type="password" name="password" id="password" @input="storeCodeLocallyDebounce"
                    placeholder="password (empty password would mean anyone can edit it)"
                    v-model="algorithmData.password">
                <button name="submit-btn" @click="submitAlgorithm()">Submit</button>
                <span class="cursor-info" :title="helpText" @click="helpVisible=!helpVisible">Need Help?</span>
            </div>
            <div class="container-item">
                <textarea ref="editorRef" class="algorithm-editor" rows="10" v-model="algorithmData.algStr"
                    @input="storeCodeLocallyDebounce"></textarea>
            </div>
            <div class="container-item">
                <span class="fs-small">Characters Count: {{algorithmData.algStr.length}}</span>
                <button name="submit-btn" @click="submitAlgorithm()">Submit</button>
            </div>
        </div>
    </div>
    <div class="right-div">
        <div class="container-row">

            <div class="container-item">
                <div class="button-bar">
                    <template v-for="(view, v) in viewList">
                        <button @click="changeTab(view)">{{view}}</button>
                    </template>
                </div>
            </div>
            <div class="container-item">
                <div class="button-bar header">
                    <div id="heading-bar">{{viewName}}</div>
                </div>
            </div>
            <div class="container-item" id="result">
                <component :is="viewName"></component>
            </div>
        </div>
    </div>
</div>


`;