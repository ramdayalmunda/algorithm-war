export default `

<div class="container">

    <div class="left-div">
        <div class="container-row">
            <div class="container-item">
                <div class="header">Algorithm Data <span class="icon" v-if="algorithmData?._id">✏️</span> <span class="fs-small animate-fade" :class="localStoreDone?'show':''"> Saving...</span></div>
            </div>
            <div class="container-item">
                <input type="text" name="name-of-algorithm" id="name-of-algorithm" v-bind:disabled="!isEdit"
                    placeholder="name for your algorithm" v-model="algorithmData.name">
                <input type="password" name="password" id="password" v-bind:disabled="!isEdit"
                    placeholder="password (empty password would mean anyone can edit it)" v-model="algorithmData.password">
                <div class="justify-content-between" v-if="!algorithmData.validated && algorithmData?._id">
                    <button class="submit-btn">Approve</button>
                    <div class="justify-content-end">
                        <button class="submit-btn me-2">Reject</button>
                        <button class="submit-btn">Cancel</button>
                    </div>
                </div>
                <span class="cursor-info" :title="helpText">Need Help?</span>
            </div>
            <div class="container-item">
                <textarea ref="editorRef" class="algorithm-editor" v-bind:disabled="!isEdit"
                    rows="10" v-model="algorithmData.algStr" @input="storeCodeLocallyDebounce"></textarea>
            </div>
            <div class="container-item">
                <span class="fs-small">Characters Count: {{algorithmData.algStr.length}}</span>
                <div class="justify-content-between" v-if="!algorithmData.validated && algorithmData?._id">
                    <button class="submit-btn">Approve</button>
                    <div class="justify-content-end">
                        <button class="submit-btn me-2">Reject</button>
                        <button class="submit-btn">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="right-div">
        <div class="container-row">

            <div class="container-item">
                <div class="button-bar">
                    <template v-for="(item, i) in viewList">
                        <button @click="changeTab(item)">{{item}}</button>
                    </template>
                </div>
            </div>
            <div class="container-item">
                <div class="button-bar header">
                    <div id="heading-bar">{{view}}</div>
                </div>
            </div>
            <div class="container-item">
                <AlgorithmList v-if="view=='AlgorithmList'"
                    :columnList="['#','name', 'load' ]" @send-algorithm-data="getAlgorithm"
                ></AlgorithmList>
            </div>
        </div>
    </div>
</div>


`;