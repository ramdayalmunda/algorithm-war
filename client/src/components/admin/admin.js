import { helpText } from "../../common/constants.js";
import template from "./admin.template.js"

import AlgorithmList from "../war/algorithm-list/algorithm-list.js";

const { ref, onMounted } = Vue;
const { useRoute, useRouter} = VueRouter;
export default {
    template,
    components: {
        AlgorithmList,
    },
    setup(){
        let route = useRoute()
        let router = useRouter()
        let algorithmList = ref([])
        let algorithmData = ref({
            _id: null,
            name: '',
            password: "",
            algStr: "",
        })
        let viewList = ref([
            "AlgorithmList"
        ])
        let view = ref(route.query.tab?route.query.tab:"AlgorithmList")
        let isEdit = ref(false)

        
        function changeTab(newTabName){
            view.value = newTabName
            router.replace({ query: { tab: newTabName } })
        }

        function getAlgorithm(data){
            algorithmData.value = { ...data }
        }

        return{
            isEdit,
            helpText,
            algorithmList,
            algorithmData,
            viewList,
            view,
            changeTab,
            getAlgorithm,
        }
    }
}