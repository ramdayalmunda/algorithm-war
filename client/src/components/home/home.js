export default {
    template: /*html*/ `
    
    <h1>Welcome to algorithm war</h1>
    <ul>
        <li>
            <router-link to="war">To War</router-link>
        </li>
        <li>
            <router-link to="admin">To Admin Section</router-link>
        </li>
    </ul>
    
    `
}