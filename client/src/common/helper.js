
export const debounce = function ( func, delay ){
    delay = delay?delay:1000;
    let timeHandler;
    return async (...args)=>{
        clearTimeout( timeHandler )
        timeHandler = setTimeout( ()=>{
            func( ...args )
        }, delay )
    }
} 