async function api(method = 'GET', url, options = {}) {
    let baseUrl = `${window.origin}/api`
    url = baseUrl+url
    const requestOptions = {
        method: method,
        headers: {
            'Content-Type': 'application/json',
            ...options?.headers
        },
        body: JSON.stringify(options?.data) // Convert the data object to JSON string
    }
    return new Promise(function (res, rej) {
        fetch(url, requestOptions).then((response) => response.json())
            .then(data => res(data))
            .catch((err) => rej(err))
    })
}

export default api