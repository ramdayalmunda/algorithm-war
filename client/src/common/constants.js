
export const helpText = `
create a function named "algorithm" that will return either true or false
The function will have only 1 argument of type object.

This Object will have the following methods available for you
1. getMyResponse()-> Boolean; this returns your response in the last round
2. getMyResponse(n)-> Boolean; this returns your response in the nth round
3. getOpponentResponse() -> Boolean; this returna your opponent's response in the last round
4. getOpponentResponse(n) -> Boolean; this returns your opponent's response in the nth round
5. getRoundIndex() -> Number; This will return the number of round you are playing in this match
`;

