import router from "./routes.js"
const app = Vue.createApp({
    components: {
    },
    setup(){
        return {
        }
    },
    template: /*html*/`
        <router-view></router-view>
    `
})

app.use(router)
app.mount("#vue-app")