const router = require('express').Router()
const tournamentController = require("../controller/tournament.js")


router.get('/scores', tournamentController.getScores)
router.post('/start', tournamentController.startTournament)


module.exports = router