const router = require('express').Router()
const algorithmController = require("../controller/algorithm.js")


router.get('/all', algorithmController.getAlgorithmList)
router.post('/', algorithmController.submitAlgorithm)


module.exports = router