const express = require('express')
const app = express()
const port = 4902;
const mongoose = require('mongoose')
global.CONFIG = require("./config.js")
const fs = require("fs");

app.use( express.json() )

app.use("/tournament", require("./routes/tournament.js"))
app.use("/algorithm", require("./routes/algorithm.js"))


app.listen( port, ()=>{
    console.log(`game running on http://localhost:${port}`)
    mongoose.connect(CONFIG.MDB.url).then(() => {
        console.log('DB connection established')
    }).catch(err => {
        console.log('Failed to connect to DB!')
    })

    // create temp folders
    Object.keys(CONFIG.TEMP).forEach( item => {
        fs.mkdir( CONFIG.TEMP[item] , (err) => {
            if (err && err?.code!='EEXIST') { console.error("Error creating folder:", err); }
        });

    } )

} )