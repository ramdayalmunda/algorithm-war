const mongoose = require("mongoose")
const collectionName = "Tournament"
const collectionSchema = new mongoose.Schema(
    {
        players: [
            {
                _id: mongoose.Types.ObjectId, // _id of the player
                name: String, // name of the player,
                score: Number, // scores in the tournament
            },
        ]

    },
    { timestamps: true }
)

module.exports = {
    collectionSchema,
    collectionName,
}