const mongoose = require("mongoose")
const collectionName = "Match"
const collectionSchema = new mongoose.Schema(
    {

        players: [
            {
                id: mongoose.Types.ObjectId,
                name: String,
                score: { type: Number, default: 0 }
            }
        ],
        tournamentId: mongoose.Types.ObjectId,
        rounds: [
            [
                Boolean, // reponse of player0
                Boolean, // reponse of player1
            ]
        ]

    },
    { timestamps: true }
)

module.exports = {
    collectionSchema,
    collectionName,
}