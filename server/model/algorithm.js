const mongoose = require("mongoose")
const collectionName = "Algorithm"
const collectionSchema = new mongoose.Schema(
    {
        name: { type: String, required: true },
        algStr: { type: String, required: true},
        validated: { type: Boolean, default: false }
    },
    { timestamps: true }
)

module.exports = {
    collectionSchema,
    collectionName,
}