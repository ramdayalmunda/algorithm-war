const model = require("../model/index.js")

module.exports.createTournament= async function (data) {
    let dbData = await model.Tournament(data)
    await dbData.save()
    return dbData
}

module.exports.updateTournament = async function (matchQuery, setQuery){
    return model.Tournament.findOneAndUpdate(matchQuery, { $set: setQuery }, { new: true })
}

module.exports.getLatestTournament = async function (){
    return model.Tournament.find().sort({ createdAt: -1 }).limit(-1)
}