const model = require("../model/index.js")

module.exports.getAllAlgorithm = async function (matchQuery) {
    return await model.Algorithm.find(matchQuery).lean()
}

module.exports.checkIfPresent = async function (matchQuery) {
    return await model.Algorithm.countDocuments(matchQuery)
}

module.exports.saveAlgorithm = async function (data) {
    return model.Algorithm.findOneAndUpdate({ _id: data._id }, data, { new: true, upsert: true })
}