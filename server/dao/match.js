const model = require("../model/index.js")

module.exports.saveMatch = async function (data) {
    let dbData = await model.Match(data)
    await dbData.save()
    return dbData
}

module.exports.getMatches = async function ( matchQuery ){
    return await model.Match.find(matchQuery)
}