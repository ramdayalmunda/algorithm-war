const algDao = require("../dao/algorithm.js")
const path = require('path')
const tourDao = require("../dao/tournament.js")
const { saveMatch, getMatches } = require("../dao/match.js")
module.exports.getScores = async (req, res) => {
    try {
        let latestTounament = await tourDao.getLatestTournament()
        latestTounament = latestTounament[0]
        let matches = await getMatches({ tournamentId: latestTounament?._id })
        res.status(200).json({ message: "Got Tournament Data", tournamentData: latestTounament, matches })
    } catch (err) {
        res.status(500).json({ message: "INTERNAL SERVER ERROR" })
    }
}

module.exports.startTournament = async function (req, res) {
    try {
        let status = await setupAndStartTournament()
        res.status(200).json({ message: status ? "Tournament is going on" : "Tournament Started" })
    } catch (err) {
        console.log(err)
        res.status(500).json({ message: "INTERNAL SERVER ERROR" })
    }
}

async function setupAndStartTournament() {

    if (global.tournamentStarted) {
        console.log('tournament already stated')
        return true
    }
    global.tournamentStartTime = new Date()

    let tournamentData = await tourDao.createTournament()
    let algorithms = await algDao.getAllAlgorithm({ validated: true })
    let algorithmList = algorithms.map(item => { return { _id: item._id, name: item.name, score: 0 } })
    await tourDao.updateTournament({ _id: tournamentData._id }, { players: algorithmList })
    let combinations = generateCombinations(algorithms)
    let reverse = false
    for (let c = 0; c < combinations.length; c++) {
        let matchIndex = reverse ? (combinations.length - c) : c
        let algorithm0 = algorithms[combinations[matchIndex][0]]
        let algorithm1 = algorithms[combinations[matchIndex][1]]
        let matchObj = {
            players: [
                { id: algorithm0._id, name: algorithm0.name, score: 0 },
                { id: algorithm1._id, name: algorithm1.name, score: 0 },
            ],
            tournamentId: tournamentData._id,
            rounds: [],
        }
        algorithm0.alg = require(path.join(CONFIG.TEMP.ALG, `${algorithm0._id}.js`))
        algorithm1.alg = require(path.join(CONFIG.TEMP.ALG, `${algorithm1._id}.js`))

        var roundIndex = 0
        var playerIndex = 0
        let gameObj = {
            getRoundIndex() { return roundIndex },
            getMyResponse(n) {
                if (typeof n == 'undefined') n = roundIndex - 1
                return matchObj.rounds[n][playerIndex]
            },
            getOpponentResponse(n) {
                if (typeof n == 'undefined') n = roundIndex - 1
                return matchObj.rounds?.[n]?.[playerIndex ? 0 : 1]
            },
        }
        for (roundIndex = 0; roundIndex < 100; roundIndex++) {
            playerIndex = 0;
            p0Decision = algorithm0.alg(gameObj)
            playerIndex = 1
            p1Decision = algorithm1.alg(gameObj)

            let roundScore = []
            if (p0Decision != p1Decision) {
                roundScore = [p0Decision ? 0 : 5, p1Decision ? 0 : 5]
            } else {
                roundScore = [p0Decision ? 1 : 0, p1Decision ? 1 : 0]
            }
            matchObj.rounds.push([p0Decision, p1Decision])
            matchObj.players[0].score += roundScore[0]
            matchObj.players[1].score += roundScore[1]
        }
        
        tournamentData.players
        await saveMatch( matchObj )
        reverse = !reverse
    }

    setTimeout( ()=>{
        global.tournamentStarted = true
    }, 1000*60*10 )

}

function generateCombinations(arr) {
    const combinations = [];

    for (let i = 0; i < arr.length; i++) {
        for (let j = i; j < arr.length; j++) {
            combinations.push([i, j]);
        }
    }

    return combinations;
}