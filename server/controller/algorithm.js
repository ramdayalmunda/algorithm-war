const mongoose = require('mongoose');
const algDao = require('../dao/algorithm.js');
const fs = require("fs");
const path = require('path');
const { getLatestTournament } = require('../dao/tournament.js');
const { getMatches } = require('../dao/match.js');
module.exports.getAlgorithmList = async (req, res) => {
    try {
        let latestTournament = await getLatestTournament()
        let algorithmList = await algDao.getAllAlgorithm()
        latestTournament = latestTournament[0]
        let matches = await getMatches({ tournamentId: latestTournament?._id })
        for(let i=0; i<matches.length; i++){
            let player0 = algorithmList.find( alg => alg._id.toString() == matches[i].players[0].id.toString() )
            player0.score = player0.score?player0.score:0
            let player1 = algorithmList.find( alg => alg._id.toString() == matches[i].players[1].id.toString() )
            player1.score = player1.score?player1.score:0
            for( let r=0; r<matches[i].rounds.length; r++ ){
                player0.score += matches[i].rounds[r][0]
                player1.score += matches[i].rounds[r][1]
            }
        }
        algorithmList = algorithmList.sort( ( a,b )=> b.score-a.score )
        res.status(200).json({ message: "alg", list: algorithmList })
    } catch (err) {
        console.log(err)
        res.status(500).json({ message: "INTERNAL SERVER ERROR" })
    }
}

module.exports.submitAlgorithm = async (req, res) => {

    try {
        let isPresent = false
        let resObj = { success: false, message: "" }
        if (!req.body._id) {
            req.body._id = new mongoose.Types.ObjectId()
            isPresent = await algDao.checkIfPresent({ name: req.body.name })
        }
        else {
            req.body._id = new mongoose.Types.ObjectId(req.body._id)
            isPresent = await algDao.checkIfPresent({ _id: { $ne: req.body._id }, name: req.body.name })
        }
        if (isPresent) {
            resObj.message = "Name is already in use."
        } else {
            resObj.success = true
            resObj.message = "Algorithm has been submitted for validation!"
            let savedData = await algDao.saveAlgorithm(req.body)
            resObj.algorithmData = { ...savedData._doc }
            req.body.algStr += `\r\n\r\nmodule.exports = algorithm`

            let fileName = path.join( CONFIG.TEMP.ALG, `${resObj.algorithmData._id}.js` )

            fs.writeFile(fileName, req.body.algStr, (err) => {
                if (err) {
                    console.error('Error writing file:', err);
                } else {
                    console.log('File written successfully:', fileName);
                }
            });
        }
        res.status(200).json(resObj)
    } catch (err) {
        console.log(err)
        res.status(500).json({ message: "INTERNAL SERVER ERROR" })
    }

}