global.CONFIG = require("../config.js")
const { getAllAlgorithm } = require("../dao/algorithm.js")
const mongoose = require('mongoose')
const fs = require('fs')
const path = require("path")

var dbConnected = false

var scripts = {
    // script to generate files for all validated algorithms
    async generateAlgorithmFiles() {
        console.log('generateAlgorithmFiles')
        let algorithms = await getAllAlgorithm({ validated: true })
        for (let a = 0; a < algorithms.length; a++) {
            let fileName = path.join(CONFIG.TEMP.ALG, `${algorithms[a]._id.toString()}.js`)
            let content = algorithms[a].algStr + '\r\n\r\nmodule.exports = algorithm'
            fs.writeFile(fileName, content, (err) => {
                if (err) {
                    console.error('Error writing file:', err);
                } else {
                    console.log('File written successfully:', fileName);
                }
            });
        }

        if (dbConnected) mongoose.disconnect()
    }
}
let scriptName = process.env.SCRIPT_NAME?.trim()
if (scriptName && scripts[scriptName]) scripts[scriptName]()

mongoose.connect(CONFIG.MDB.url).then(async () => {
    console.log('DB connection established!')
    dbConnected = true
}).catch(err => {
    console.log(err)
})