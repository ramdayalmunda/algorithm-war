const path = require('path')
const CONFIG = {
    SERVER_PORT: 4902,
    MDB: {
        url: "mongodb://127.0.0.1:27017/algowar"
    },
    TEMP: {
        BASE: path.join(__dirname, 'temporary'),
        ALG: path.join(__dirname, 'temporary', 'algorithm'),
    }
}

module.exports = CONFIG