global.CONFIG = require("../config.js")
const model = require("../model/index.js")
const mongoose = require('mongoose')
const seedList = [
    {
        model: "Algorithm",
        clear: false,
        replace: true,
        dataList: [
            {
                _id: new mongoose.Types.ObjectId("658814c2c09f0aa608ae3e73"),
                name: "I'll Play Random",
                validated: true,
                algStr: `
function algorithm ( gameObj ){
  return !Math.floor( Math.random()*2 ) 
}
`,
            },
            {
                _id: new mongoose.Types.ObjectId("658814c2c09f0aa608ae3e75"),
                name: "Not so smart",
                validated: true,
                algStr: `
function algorithm (  gameObj ){
  let lastRound = gameObj.getRoundIndex()
  if (lastRound==0){
    return true
  }else{
    let cooperationScore = 0
    for (let i=0; i<lastRound; i++){
      cooperationScore += gameObj.getOpponentResponse(i)
    }
    return (cooperationScore*2)>lastRound
  }
}    
`,
            },
            
            {
                _id: new mongoose.Types.ObjectId("658814c2c09f0aa608ae3e79"),
                name: "Odd one Out",
                validated: true,
                algStr: `
function algorithm ( gameObj ){
  return gameObj.getRoundIndex()%2==0?true:false
}
`,
            },

            {
                _id : new mongoose.Types.ObjectId("65883805a6648e9740d06c20"),
                algStr : `
function algorithm (gameObj){
  return true;
};
`,
                name : "Always Co-Operate",
                validated : true
            }
        ]
    }
]

mongoose.connect(CONFIG.MDB.url).then(async () => {
    console.log('DB connection established')


    for(let i=0; i<seedList.length; i++){

        for (let j=0; j<seedList[i].dataList.length; j++){
            let dataFound = await model[seedList[i].model].findOne( { _id: seedList[i].dataList[j]._id } )
            if (dataFound){
                await model[seedList[i].model].updateOne({ _id: seedList[i].dataList[j]._id }, { $set: seedList[i].dataList[j] })
            }else{
                let dataSet = await model[seedList[i].model]( seedList[i].dataList[j] )
                await dataSet.save()
            }
        }
        console.log('Added:', seedList[i].model)
    }

    mongoose.disconnect()

}).catch(err => {
    console.log('Failed to connect to DB!', err)
})